module Allergies (Allergen(..), allergies, isAllergicTo) where

import Data.Maybe (fromJust)

data Allergen = Eggs
              | Peanuts
              | Shellfish
              | Strawberries
              | Tomatoes
              | Chocolate
              | Pollen
              | Cats
              deriving (Eq, Show)

allergies :: Int -> [Allergen]
allergies score = reverse $ map lookupAllergen (decomposeScore score' scores)
    where score'           = min score (sum scores)
          lookupAllergen s = fromJust $ lookup s scoredAllergen

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo allergen score = allergen `elem` (allergies score)

decomposeScore :: Int -> [Int] -> [Int]
decomposeScore 0 _ = []
decomposeScore n (x:xs)
    | n >= x       = x : decomposeScore (n - x) xs
    | otherwise    = decomposeScore n xs

scores :: [Int]
scores = reverse $ map (\t -> fst t) scoredAllergen

scoredAllergen :: [(Int, Allergen)]
scoredAllergen = [(1, Eggs), (2, Peanuts), (4, Shellfish),
                  (8, Strawberries), (16, Tomatoes), (32, Chocolate),
                  (64, Pollen), (128, Cats)]
