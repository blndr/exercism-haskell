module Triplet (tripletsWithSum) where

tripletsWithSum :: Int -> [(Int, Int, Int)]
tripletsWithSum n = [(a, b, c)
                    | b <- [4..n `div` 2], a <- [3..b],
                    let c = n - a - b,
                        a * a + b * b == c * c]
