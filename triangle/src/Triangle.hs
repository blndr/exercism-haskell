module Triangle (TriangleType(..), triangleType) where

data TriangleType = Equilateral
                  | Isosceles
                  | Scalene
                  | Illegal
                  deriving (Eq, Show)

triangleType :: (Ord a, Eq a, Num a) => a -> a -> a -> TriangleType
triangleType a b c
    | or [b + c < a, a + b < c, any (== 0) [a, b, c]] = Illegal
    | a == b && b == c           = Equilateral
    | a /= b && b /= c && a /= c = Scalene
    | otherwise                  = Isosceles