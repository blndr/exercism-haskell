module Anagram (anagramsFor) where

import Data.List (sort)
import Data.Char (toLower)

anagramsFor :: String -> [String] -> [String]
anagramsFor search text = filter (isAnagram search) text
    where
        isAnagram word token = checksum word == checksum token && normalize word /= normalize token
        normalize word = map toLower word
        checksum = sort . normalize
