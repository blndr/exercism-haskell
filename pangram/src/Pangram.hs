module Pangram (isPangram) where

import Data.Char (toLower)

isPangram :: String -> Bool
isPangram text = all (isIn (map toLower text)) ['a'..'z']
    where isIn phrase letter = letter `elem` phrase
