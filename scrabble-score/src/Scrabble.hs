module Scrabble (scoreLetter, scoreWord) where

import Data.Char (toLower)

points :: [(String, Int)]
points = [
            ("aeioulnrst", 1),
            ("dg", 2),
            ("bcmp", 3),
            ("fhvwy", 4),
            ("k", 5),
            ("jx", 8),
            ("qz", 10)
         ]

findScore :: Char -> (String, Int) -> Int
findScore char (letters, score)
    | (toLower char) `elem` letters = score
    | otherwise                     = 0

scoreLetter :: Char -> Integer
scoreLetter c = (toInteger . sum) $ map (findScore c) points

scoreWord :: String -> Integer
scoreWord word = sum $ map scoreLetter word
