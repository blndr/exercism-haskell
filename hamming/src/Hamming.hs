module Hamming (distance) where

distance :: String -> String -> Maybe Int
distance xs ys
    | length xs /= length ys = Nothing
    | otherwise              = Just (length $ (differingNucleotides xs ys))
    where
        differingNucleotides as bs = filter (not . isEqual) (zip as bs)
        isEqual (a, b)             = a == b
