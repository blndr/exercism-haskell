module Brackets (arePaired) where

arePaired :: String -> Bool
arePaired code = null $ foldl processBracket "" $ filter (\c -> c `elem` "[{()}]") code

processBracket :: String -> Char -> String
processBracket [] b = [b]
processBracket brackets b
    | isMatching (last brackets) b = init brackets
    | otherwise                    = brackets ++ [b]

isMatching :: Char -> Char -> Bool
isMatching '(' ')' = True
isMatching '[' ']' = True
isMatching '{' '}' = True
isMatching _ _     = False
