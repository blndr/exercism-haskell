module ArmstrongNumbers (armstrong) where

armstrong :: Integral a => a -> Bool
armstrong n = (armstrongValue $ splitIntoDigits n) == n

armstrongValue :: Integral a => [a] -> a
armstrongValue digits = sum $ map (^ (length $ digits)) digits

splitIntoDigits :: Integral a => a -> [a]
splitIntoDigits 0 = [0]
splitIntoDigits x = splitIntoDigits' x
    where
        splitIntoDigits' :: Integral a => a -> [a]
        splitIntoDigits' 0 = []
        splitIntoDigits' y = splitIntoDigits' (y `div` 10) ++ [y `mod` 10]

