module Queens (boardString, canAttack) where

import Data.List (intersperse)

boardString :: Maybe (Int, Int) -> Maybe (Int, Int) -> String
boardString wq bq = ((formatBoard 0) . (putQueenOnBoard bq 'B') . (putQueenOnBoard wq 'W')) emptyBoard

emptyBoard :: String
emptyBoard = replicate 64 '_'

putQueenOnBoard :: Maybe (Int, Int) -> Char -> String -> String
putQueenOnBoard Nothing _ b       = b
putQueenOnBoard (Just (r, c)) q b = start ++ [q] ++ remainer 
    where
        start    = take i b
        remainer = (take (64 - i) . (drop i)) b
        i        = 8 * r + c

formatBoard :: Int -> String -> String
formatBoard 64 _         = ""
formatBoard offset board = line ++ formatBoard (offset + 8) board
    where line = (intersperse ' ' $ ((take 8) . (drop offset)) board) ++ "\n"

canAttack :: (Int, Int) -> (Int, Int) -> Bool
canAttack (wr, wc) (br, bc)
    | wr == br                     = True
    | wc == bc                     = True
    | abs(wr - br) == abs(wc - bc) = True
    | otherwise                    = False
