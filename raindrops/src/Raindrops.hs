module Raindrops (convert) where

import Data.Maybe (fromJust, isJust)

convert :: Int -> String
convert n
    | not $ any (\p -> n `mod` p == 0) [3, 5, 7] = show n
    | otherwise = concat $ map fromJust $ filter isJust $ map (rainDrop n) possibilities

possibilities :: [(Int, String)]
possibilities = [(3, "Pling"), (5, "Plang"), (7, "Plong")]

rainDrop :: Int -> (Int, String) -> Maybe String
rainDrop n (p, noise)
    | n `mod` p == 0 = Just noise
    | otherwise      = Nothing