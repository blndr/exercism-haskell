module Diamond (diamond) where

diamond :: Char -> Maybe [String]
diamond l = Just (quadrant l)

quadrant :: Char -> [String]
quadrant l = map (row l) ['A'..l] ++ map (row l) (reverse $ init ['A'..l])

row :: Char -> Char -> String
row lastLetter l = leftPart lastLetter l ++ tail (reverse $ leftPart lastLetter l)

leftPart :: Char -> Char -> String
leftPart lastLetter l = replicate (charIndex lastLetter - charIndex l) ' ' ++ [l] ++ replicate (charIndex l) ' '

charIndex :: Char -> Int
charIndex l = length ['A'..l] - 1