module CustomSet
  ( delete
  , difference
  , empty
  , fromList
  , insert
  , intersection
  , isDisjointFrom
  , isSubsetOf
  , member
  , null
  , size
  , toList
  , union
  ) where

import Prelude hiding (null)

data CustomSet a = CS {values :: [a]} deriving (Eq, Show)

delete :: (Ord a, Eq a) => a -> CustomSet a -> CustomSet a
delete x set = difference set (CS [x])

difference :: (Ord a, Eq a) => CustomSet a -> CustomSet a -> CustomSet a
difference setX setY = CS $ filter (\x -> not (x `elem` (values setY))) (values setX) 

empty :: CustomSet a
empty = CS []

fromList :: Ord a => [a] -> CustomSet a
fromList = foldr insert empty

insert :: Ord a => a -> CustomSet a -> CustomSet a
insert x (CS []) = CS [x]
insert x (CS (y:ys))
  | x < y  = CS (x:y:ys)
  | x == y = CS (y:ys)
  | x > y  = CS $ y : values (insert x (CS ys))

intersection :: (Ord a, Eq a) => CustomSet a -> CustomSet a -> CustomSet a
intersection setX setY = difference setX (difference setX setY)

isDisjointFrom :: (Ord a, Eq a) => CustomSet a -> CustomSet a -> Bool
isDisjointFrom setX setY = difference setX setY == setX 

isSubsetOf :: (Ord a, Eq a) => CustomSet a -> CustomSet a -> Bool
isSubsetOf setX setY = intersection setX setY == setX

member :: Eq a => a -> CustomSet a -> Bool
member x set = x `elem` (values set)

null :: Eq a => CustomSet a -> Bool
null set = set == empty 

size :: CustomSet a -> Int
size = length . values

toList :: CustomSet a -> [a]
toList = values

union :: Ord a => CustomSet a -> CustomSet a -> CustomSet a
union setX setY = foldr insert setX (values setY)
