module School (School, add, empty, grade, sorted) where

import Data.List (sort, sortBy)

data School = School [(Int, String)]

add :: Int -> String -> School -> School
add gradeNum student (School elements) = School (elements ++ [(gradeNum, student)])

empty :: School
empty = School []

grade :: Int -> School -> [String]
grade gradeNum (School elements) = map pluckName (filter (matchesGrade gradeNum) elements)
    where
        pluckName (_, n) = n
        matchesGrade baseGrade (g, _) = g == baseGrade

sorted :: School -> [(Int, [String])]
sorted (School elements) = foldl mergeOnGrade [] (sortBy compareStudent elements)
    where compareStudent (a, _) (b, _) = compare a b

mergeOnGrade :: [(Int, [String])] -> (Int, String) -> [(Int, [String])]
mergeOnGrade [] (gradeNum, name) = [(gradeNum, [name])]
mergeOnGrade [(g, ns)] (gradeNum, name)
    | g == gradeNum = [(g, sort $ ns ++ [name])]
    | otherwise     = [(g, ns), (gradeNum, [name])]
mergeOnGrade ((gA, nsA):(gB, nsB):xs) (gradeNum, name)
    | gA == gB  = [(gA, sort $ nsA ++ nsB)] ++ mergeOnGrade xs (gradeNum, name)
    | otherwise = [(gA, nsA)] ++ mergeOnGrade ([(gB, nsB)] ++ xs) (gradeNum, name)
