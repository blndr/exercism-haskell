module DNA (toRNA) where

import Data.Maybe (fromJust, isJust)

toRNA :: String -> Either Char String
toRNA ""  = Right ""
toRNA dna
    | isDnaValid dna = Right (map (fromJust . complement) dna)
    | otherwise      = Left (fromJust $ findInvalidNucleotide dna)

findInvalidNucleotide :: String -> Maybe Char
findInvalidNucleotide [] = Nothing
findInvalidNucleotide [x]
    | isJust $ complement x = Nothing
    | otherwise             = Just x
findInvalidNucleotide (x:xs)
    | isJust $ findInvalidNucleotide [x] = findInvalidNucleotide [x]
    | otherwise                          = findInvalidNucleotide xs

isDnaValid :: String -> Bool
isDnaValid []     = True
isDnaValid [x]    = x `elem` "ACTG"
isDnaValid (x:xs) = isDnaValid [x] && isDnaValid xs

complement :: Char -> Maybe Char
complement 'G' = Just 'C'
complement 'C' = Just 'G'
complement 'T' = Just 'A'
complement 'A' = Just 'U'
complement _   = Nothing 
