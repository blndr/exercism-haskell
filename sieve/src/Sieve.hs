module Sieve (primesUpTo) where

-- You should not use any of the division operations when implementing
-- the sieve of Eratosthenes.
import Prelude hiding (div, mod, divMod, rem, quotRem, quot, (/))
import Data.List

primesUpTo :: Integer -> [Integer]
primesUpTo 1 = []
primesUpTo n = sieve [2..n] 2

sieve :: [Integer] -> Integer -> [Integer]
sieve numbers n
    | n == maximum numbers = numbers
    | otherwise        = sieve (numbers \\ (multiplesOf n numbers)) (n + 1)

multiplesOf :: Integer -> [Integer] -> [Integer]
multiplesOf n numbers = tail [n,n + n..maximum numbers]
