module Series (slices) where

import Data.Char (digitToInt)

slices :: Int -> String -> [[Int]]
slices size word = map ((map digitToInt) . (sublist word size)) [0..length word - size]

sublist :: String -> Int -> Int -> String
sublist word size offset = (take size . drop offset) word
