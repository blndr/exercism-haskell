module PrimeFactors (primeFactors) where

primeFactors :: Integer -> [Integer]
primeFactors n = primeFactors' n 2

primeFactors' :: Integer -> Integer -> [Integer]
primeFactors' 1 _ = []
primeFactors' n f
    | mod n f == 0 = f : primeFactors' (div n f) f
    | otherwise    = primeFactors' n (f + 1)
