module Prime (nth) where

import Data.Maybe (fromJust)

nth :: Int -> Maybe Integer
nth 0 = Nothing
nth 1 = Just 2
nth n = (Just . toInteger . head . (filter isPrime)) [lastInteger + 1..]
    where lastInteger = fromInteger (fromJust (nth (n -1)))

isPrime :: Int -> Bool
isPrime n = all (\d -> n `mod` d /= 0) [2..maxDivisor]
    where maxDivisor = floor $ sqrt (fromIntegral n)
