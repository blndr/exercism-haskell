module LinkedList
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

data LinkedList a = Empty | Cons a (LinkedList a) deriving (Eq, Show)

datum :: LinkedList a -> a
datum Empty = error "Empty LinkedList"
datum (Cons x _) = x

fromList :: [a] -> LinkedList a
fromList [] = Empty
fromList (x:xs) = new x (fromList xs)

isNil :: LinkedList a -> Bool
isNil Empty = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new x linkedList = Cons x linkedList

next :: LinkedList a -> LinkedList a
next Empty = Empty
next (Cons _ xs) = xs

nil :: LinkedList a
nil = Empty

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList = fromList . reverse . toList

toList :: LinkedList a -> [a]
toList Empty = []
toList (Cons x xs) = [x] ++ (toList xs)
