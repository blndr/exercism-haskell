module LeapYear (isLeapYear) where

dividable :: Integer -> Integer -> Bool
dividable n p = n `mod` p == 0

isLeapYear :: Integer -> Bool
isLeapYear year
  | year `dividable` 400 = True
  | year `dividable` 100 = False
  | year `dividable` 4 = True
  | otherwise = False
