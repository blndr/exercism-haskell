module Matrix
    ( Matrix
    , cols
    , column
    , flatten
    , fromList
    , fromString
    , reshape
    , row
    , rows
    , shape
    , transpose
    ) where

import qualified Data.Vector as V (Vector, fromList, concat, toList, empty, cons, (!), reverse)

type Matrix a = V.Vector (V.Vector a)

cols :: Matrix a -> Int
cols matrix
    | length matrix == 0 = 0
    | otherwise = length (matrix V.! 0)

column :: Int -> Matrix a -> V.Vector a
column x matrix = V.reverse $ foldl (\acc v -> V.cons (v V.! x) acc) V.empty matrix

flatten :: Matrix a -> V.Vector a
flatten matrix = V.concat $ map (\i -> row i matrix) [0..(rows matrix) - 1]

fromList :: [[a]] -> Matrix a
fromList xss = V.fromList $ map V.fromList xss

fromString :: String -> Matrix Int
fromString xs = fromList $ map ((map read) . words) (lines xs)

reshape :: (Int, Int) -> Matrix a -> Matrix a
reshape (_, c) matrix = fromList $ makeRows 0 ((V.toList . V.concat) (V.toList matrix))
    where
        makeRows :: Int -> [a] -> [[a]]
        makeRows offset xs
            | offset >= length xs = []
            | otherwise           = (((take c) . (drop offset)) xs) : makeRows (offset + c) xs

row :: Int -> Matrix a -> V.Vector a
row x matrix = matrix V.! x

rows :: Matrix a -> Int
rows matrix = length matrix

shape :: Matrix a -> (Int, Int)
shape matrix = (rows matrix, cols matrix)

transpose :: Matrix a -> Matrix a
transpose matrix = V.fromList $ map (\i -> column i matrix) [0..(cols matrix) - 1]
