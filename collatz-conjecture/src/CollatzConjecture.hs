module CollatzConjecture (collatz) where

collatz :: Integer -> Maybe Integer
collatz n
    | n <= 0 = Nothing
    | otherwise = Just (nextStep n 0)

nextStep :: Integer -> Integer -> Integer
nextStep n c
    | n == 1 = c
    | even n = nextStep (n `div` 2) (c + 1)
    | odd n = nextStep (n * 3 + 1) (c + 1)
