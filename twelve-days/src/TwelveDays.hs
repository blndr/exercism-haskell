module TwelveDays (recite) where

import Text.Printf

verseTemplate = "On the %s day of Christmas my true love gave to me: %s."

recite :: Int -> Int -> [String]
recite start stop = map verse [start..stop]

verse :: Int -> String
verse n = printf verseTemplate (days !! (n - 1)) (buildGifts $ (reverse . take n) gifts)

buildGifts :: [String] -> String
buildGifts []     = []
buildGifts [x]    = x
buildGifts [x, y] = x ++ ", and " ++ y
buildGifts (x:xs) = x ++ ", " ++ buildGifts xs

days :: [String]
days =
    [
        "first", "second", "third", "fourth", "fifth", "sixth",
        "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"
    ]

gifts :: [String] 
gifts = 
    [
        "a Partridge in a Pear Tree", "two Turtle Doves", "three French Hens",
        "four Calling Birds", "five Gold Rings", "six Geese-a-Laying",
        "seven Swans-a-Swimming", "eight Maids-a-Milking", "nine Ladies Dancing",
        "ten Lords-a-Leaping", "eleven Pipers Piping", "twelve Drummers Drumming"
    ]
