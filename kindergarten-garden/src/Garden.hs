module Garden
    ( Plant (..)
    , garden
    , lookupPlants
    ) where

import Data.List (elemIndex, find, lookup)
import Data.Maybe (fromJust)

data Plant = Clover | Grass | Radishes | Violets deriving (Eq, Show)
data Garden = Garden {values :: [(String, [Plant])]}

makePlant :: Char -> Plant
makePlant p = fromJust $ lookup p [('R', Radishes), ('G', Grass), ('V', Violets), ('C', Clover)]

gardenByIndex :: Int -> Int -> String -> [Plant]
gardenByIndex indexOfStudent rowLength plants = map generatePlant [0, 1, rowLength + 0, rowLength + 1]
    where generatePlant index = makePlant $ plants !! (index + 2 * indexOfStudent)

studentGarden :: [String] -> String -> String -> (String, [Plant])
studentGarden students plants student = (student, gardenByIndex indexOfStudent rowLength linedUpPlants)
    where
        indexOfStudent = fromJust $ student `elemIndex` students
        rowLength = (length plants - 1) `div` 2
        linedUpPlants = filter (`elem` "CGVR") plants

garden :: [String] -> String -> Garden
garden students plants = Garden $ map (studentGarden students plants) students

lookupPlants :: String -> Garden -> [Plant]
lookupPlants student plants = snd $ fromJust $ find (\(s, _) -> s == student) (values plants)
