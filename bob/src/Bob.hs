module Bob (responseFor) where

import Data.Char (isUpper, isLetter, isSpace)

responseFor :: String -> String
responseFor str
    | isEmpty str = "Fine. Be that way!"
    | isAllUpper str && isQuestion str = "Calm down, I know what I'm doing!"
    | isAllUpper str = "Whoa, chill out!"
    | isQuestion str = "Sure."
    | otherwise = "Whatever."

isAllUpper :: String -> Bool
isAllUpper str
    | filter isLetter str == "" = False
    | all isUpper (filter isLetter str) = True
    | otherwise = False

isQuestion :: String -> Bool
isQuestion str = last (filter (not . isSpace) str) == '?'

isInjonction :: String -> Bool
isInjonction str = last str == '!'

isEmpty :: String -> Bool
isEmpty str
    | str == "" = True
    | all isSpace str = True
    | otherwise = False
    