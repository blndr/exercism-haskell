module ComplexNumbers
(Complex,
 conjugate,
 abs,
 exp,
 real,
 imaginary,
 mul,
 add,
 sub,
 div,
 complex) where

import Prelude hiding (div, abs, exp)
import qualified Prelude as P

-- Data definition -------------------------------------------------------------
data Complex a = Complex (a, a) deriving(Eq, Show)

complex :: (a, a) -> Complex a
complex (a, b) = Complex (a, b)

-- unary operators -------------------------------------------------------------
conjugate :: Num a => Complex a -> Complex a
conjugate (Complex (a, b)) = Complex (a, -b)

abs :: Floating a => Complex a -> a
abs c = sqrt $ (real c) ** 2 + (imaginary c) ** 2

real :: Num a => Complex a -> a
real (Complex (a, _)) = a

imaginary :: Num a => Complex a -> a
imaginary (Complex (_, b)) = b

exp :: (Eq a, Floating a) => Complex a -> Complex a
exp c = Complex ((P.exp . real) c, 0) `mul` Complex ((cos . imaginary) c, (sin . imaginary) c)

-- binary operators ------------------------------------------------------------
mul :: Num a => Complex a -> Complex a -> Complex a
mul (Complex (a, b)) (Complex (c, d)) = Complex (a * c - b * d, b * c + a * d)

add :: Num a => Complex a -> Complex a -> Complex a
add (Complex (a, b)) (Complex (c, d)) = Complex (a + c, b + d)

sub :: Num a => Complex a -> Complex a -> Complex a
sub (Complex (a, b)) (Complex (c, d)) = Complex (a - c, b - d)

div :: Fractional a => Complex a -> Complex a -> Complex a
div (Complex (a, b)) (Complex (c, d)) = Complex ((a * c + b * d) / (c ^ 2 + d ^ 2), (b * c - a * d) / (c ^ 2 + d ^ 2))
