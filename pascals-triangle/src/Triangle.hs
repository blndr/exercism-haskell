module Triangle (rows) where

rows :: Int -> [[Integer]]
rows 0 = []
rows 1 = [[1]]
rows n = rows (n - 1) ++ [newRow (last $ rows (n - 1))]

newRow :: [Integer] -> [Integer]
newRow row = map (compute row) [0..(length row)]

compute :: [Integer] -> Int -> Integer
compute row n
    | n == 0          = 1
    | n == length row = 1
    | otherwise       = row !! n + row !! (n - 1)
