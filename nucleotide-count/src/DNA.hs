module DNA (nucleotideCounts, Nucleotide(..)) where

import Data.Map (Map, fromList)

data Nucleotide = A | C | G | T deriving (Eq, Ord, Show)

nucleotideCounts :: String -> Either String (Map Nucleotide Int)
nucleotideCounts dna
    | any (\x -> not $ x `elem` "ACTG") dna = Left "error"
    | otherwise = Right $ fromList $ map (count dna) [A, C, T, G]

count :: String -> Nucleotide -> (Nucleotide, Int)
count dna nucleotide = (nucleotide, length $ filter (== (head $ show nucleotide)) dna)