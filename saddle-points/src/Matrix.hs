module Matrix (saddlePoints) where

import Data.Array (Array, Ix, assocs)

saddlePoints :: (Ix r, Ix c, Ord e) => Array (r, c) e -> [(r, c)]
saddlePoints m = ((map fst) . (filter isSaddle)) points
    where
        isSaddle ((r, c), e) = all (<= e) (getRow r) && all (>= e) (getCol c)
        getRow at            = map snd $ filter (\p -> (fst . fst) p == at) points  
        getCol at            = map snd $ filter (\p -> (snd . fst) p == at) points
        points               = assocs m
