module Proverb(recite) where

recite :: [String] -> String
recite []  = []
recite [x] = "And all for the want of a " ++ x ++ "."
recite xs  = concat $ map verse (pairs xs) ++ [recite [head xs]]

verse :: (String, String) -> String
verse (a, b) = "For want of a " ++ a ++ " the " ++ b ++ " was lost.\n"

pairs :: [String] -> [(String, String)]
pairs []       = []
pairs (x:y:[]) = [(x, y)]
pairs xs       = (head xs, (head . tail) xs) : pairs (tail xs)
