module Isogram (isIsogram) where

import Data.Char (toLower, isLetter)

isIsogram :: String -> Bool
isIsogram []  = True
isIsogram [_] = True
isIsogram (x:xs)
    | isLetter x = x `isNotIn` xs && isIsogram xs
    | otherwise  = isIsogram xs
    where
        isNotIn l ls = not $ toLower l `elem` map toLower ls