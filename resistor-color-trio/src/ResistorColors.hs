module ResistorColors (Color(..), Resistor(..), label, ohms) where

data Color =
    Black
  | Brown
  | Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Violet
  | Grey
  | White
  deriving (Show, Enum, Bounded)

newtype Resistor = Resistor { bands :: (Color, Color, Color) }
  deriving Show

label :: Resistor -> String
label resistor
    | numValue < 10 ^ 3 = show numValue ++ " ohms"
    | numValue < 10 ^ 6 = show (numValue `div` 10 ^ 3) ++ " kiloohms"
    | numValue < 10 ^ 9 = show (numValue `div` 10 ^ 6) ++ " megaohms"
    | otherwise = show (numValue `div` 10 ^ 9) ++ " gigaohms"
    where numValue = ohms resistor

ohms :: Resistor -> Int
ohms (Resistor(a, b, c)) = (fromEnum a * 10 + fromEnum b) * (10 ^ fromEnum c)
