module RotationalCipher (rotate) where

import Data.Char (ord, chr, isLetter, isUpper)

rotate :: Int -> String -> String
rotate n plain = map (rotateChar n) plain
    where
        rotateChar i c
            | isLetter c = chr $ ((ord c + i) - base c) `mod` 26 + base c
            | otherwise  = c
        base c = if isUpper c then ord 'A' else ord 'a'