module ProteinTranslation(proteins) where

import Data.Maybe (fromJust, isJust)

proteins :: String -> Maybe [String]
proteins chain = Just $ map fromJust $ takeWhile isJust $ map translate (chunksOf 3 chain)

chunksOf :: Int -> String -> [String]
chunksOf _ [] = []
chunksOf n sentence = (take n sentence) : (chunksOf 3 (drop n sentence))

translate :: String -> Maybe String
translate codon
    | codon == "AUG" = Just "Methionine"
    | codon == "UUU" = Just "Phenylalanine"
    | codon == "UUC" = Just "Phenylalanine"
    | codon == "UUA" = Just "Leucine"
    | codon == "UUG" = Just "Leucine"
    | codon == "UCU" = Just "Serine"
    | codon == "UCC" = Just "Serine"
    | codon == "UCA" = Just "Serine"
    | codon == "UCG" = Just "Serine"
    | codon == "UAU" = Just "Tyrosine"
    | codon == "UAC" = Just "Tyrosine"
    | codon == "UGU" = Just "Cysteine"
    | codon == "UGC" = Just "Cysteine"
    | codon == "UGG" = Just "Tryptophan"
    | otherwise      = Nothing
