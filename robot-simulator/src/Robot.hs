module Robot
    ( Bearing(East,North,South,West)
    , bearing
    , coordinates
    , mkRobot
    , move
    ) where

import Data.Maybe (fromJust)
import Data.List (elemIndex)

data Bearing = North
             | East
             | South
             | West
             deriving (Eq, Show)

data Robot = Robot Bearing (Integer, Integer)

rightBearings :: [Bearing]
rightBearings = cycle [North, East, South, West]

leftBearings :: [Bearing]
leftBearings = cycle [North, West, South, East]

advanceCoordinates :: [(Bearing, (Integer, Integer) -> (Integer, Integer))]
advanceCoordinates = [
        (North, \(x, y) -> (x, y + 1)), (East, \(x, y) -> (x + 1, y)),
        (South, \(x, y) -> (x, y - 1)), (West, \(x, y) -> (x - 1, y))
    ]

bearing :: Robot -> Bearing
bearing (Robot b _) = b

newBearing :: [Bearing] -> Bearing -> Bearing
newBearing oriented current = oriented !! (1 + (fromJust $ current `elemIndex` oriented))

coordinates :: Robot -> (Integer, Integer)
coordinates (Robot _ (x, y)) = (x, y)

mkRobot :: Bearing -> (Integer, Integer) -> Robot
mkRobot direction coords = Robot direction coords

move :: Robot -> String -> Robot
move r [] = r
move r ['R'] = mkRobot (newBearing rightBearings (bearing r)) (coordinates r)
move r ['L'] = mkRobot (newBearing leftBearings (bearing r)) (coordinates r)
move r ['A'] = mkRobot (bearing r) ((fromJust $ lookup (bearing r) advanceCoordinates) (coordinates r))
move r (x:xs) = move (move r [x]) xs
