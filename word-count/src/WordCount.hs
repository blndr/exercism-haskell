module WordCount (wordCount) where

import Data.List (nub)
import Data.Char (isAlphaNum, toLower)

wordCount :: String -> [(String, Int)]
wordCount text = [(word, count word) | word <- (nub text')]
    where
        text'      = ((map cleanQuotes) . words . tokenify) text
        count word = length $ filter (== word) text'

cleanQuotes :: String -> String
cleanQuotes word
    | (isAlphaNum . head) word && (isAlphaNum . last) word = word
    | otherwise                                            = (tail . init) word     

tokenify :: String -> String
tokenify []     = []
tokenify [x]    = if isAlphaNum x || x == '\'' then [toLower x] else " "
tokenify (x:xs) = tokenify [x] ++ tokenify xs
