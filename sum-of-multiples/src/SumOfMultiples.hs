module SumOfMultiples (sumOfMultiples) where

import Data.List (nub)

sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples [] _          = 0
sumOfMultiples _ 1           = 0
sumOfMultiples factors limit = (sum . nub . concat) $ map (multiplesBelow limit) factors

multiplesBelow :: Integer -> Integer -> [Integer]
multiplesBelow _ 0     = []
multiplesBelow limit n = tail [0, n..limit-1]
