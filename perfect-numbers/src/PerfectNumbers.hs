module PerfectNumbers (classify, Classification(..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

classify :: Int -> Maybe Classification
classify n
    | n < 1                     = Nothing
    | sum (findDivisors n) > n  = Just Abundant
    | sum (findDivisors n) == n = Just Perfect
    | sum (findDivisors n) < n  = Just Deficient
    | otherwise                 = Nothing
    where
        findDivisors k = filter ((== 0) . (mod k)) [1..(k `div` 2)]