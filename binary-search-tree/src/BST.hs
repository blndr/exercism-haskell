module BST
    ( BST
    , bstLeft
    , bstRight
    , bstValue
    , empty
    , fromList
    , insert
    , singleton
    , toList
    ) where

data BST a = Empty | Node a (BST a) (BST a) deriving (Eq, Show)

bstLeft :: BST a -> Maybe (BST a)
bstLeft Empty            = Nothing
bstLeft (Node _ Empty _) = Nothing
bstLeft (Node _ left _)  = Just left

bstRight :: BST a -> Maybe (BST a)
bstRight Empty            = Nothing
bstRight (Node _ _ Empty) = Nothing
bstRight (Node _ _ right) = Just right

bstValue :: BST a -> Maybe a
bstValue Empty        = Nothing
bstValue (Node x _ _) = Just x

empty :: BST a
empty = Empty

fromList :: Ord a => [a] -> BST a
fromList xs = foldl (\tree x -> insert x tree) Empty xs

insert :: Ord a => a -> BST a -> BST a
insert x Empty = Node x Empty Empty
insert x (Node n left right)
    | x <= n    = Node n (insert x left) right
    | x > n     = Node n left (insert x right)
    | otherwise = Node x left right

singleton :: a -> BST a
singleton x = Node x Empty Empty

toList :: BST a -> [a]
toList Empty               = []
toList (Node x left right) = toList left ++ [x] ++ toList right
