module Phone (number) where

import Data.Char (isDigit)

number :: String -> Maybe String
number xs
    | (validateNumber . pluckDigits) xs = Just (pluckDigits xs)
    | otherwise                         = Nothing
    
validateNumber :: String -> Bool
validateNumber xs = validExchangeCode xs && validAreaCode xs && validLength xs
    where
        validLength number       = length number == 10
        validAreaCode number     = (read . take 1) number > 1
        validExchangeCode number = (read . take 1 . drop 3) number > 1

pluckDigits :: String -> String
pluckDigits [] = []
pluckDigits (x:xs)
    | not $ isDigit x = pluckDigits xs
    | x == '1'        = filter isDigit xs
    | otherwise       = filter isDigit (x:xs)