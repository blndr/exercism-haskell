module Acronym (abbreviate) where

import Data.Char (isUpper, toUpper)

abbreviate :: String -> String
abbreviate phrase = concat $ map acronymize (words $ replaceDash phrase)

replaceDash :: String -> String
replaceDash [] = ""
replaceDash [x]
    | x == '-'  = " "
    | otherwise = [x]
replaceDash (x:xs) = replaceDash [x] ++ replaceDash xs

acronymize :: String -> String
acronymize word
    | all isUpper word = take 1 word
    | any isUpper word = filter isUpper word
    | otherwise = map toUpper $ take 1 word
