module Clock (addDelta, fromHourMin, toString) where

data Clock = Clock { hour :: Int, minutes :: Int } deriving (Eq, Show)

fromHourMin :: Int -> Int -> Clock
fromHourMin h m = realClock $ Clock h m

toString :: Clock -> String
toString clock = (printValue . hour . realClock) clock
                    ++ ":"
                    ++ (printValue . minutes . realClock) clock

realClock :: Clock -> Clock
realClock clock = Clock realMinutes realHours
    where
        addedHours  = (minutes clock) `div` 60
        realHours   = (minutes clock) `mod` 60
        realMinutes = (hour clock + addedHours) `mod` 24

printValue :: Int -> String
printValue value
    | value < 10 = '0' : show value
    | otherwise  = show value

addDelta :: Int -> Int -> Clock -> Clock
addDelta h m c = realClock $ Clock (hour c + h) (minutes c + m)
