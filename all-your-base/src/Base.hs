module Base (Error(..), rebase) where

data Error a = InvalidInputBase | InvalidOutputBase | InvalidDigit a
    deriving (Show, Eq)

rebase :: Integral a => a -> a -> [a] -> Either (Error a) [a]
rebase i o ds
    | i <= 1             = Left InvalidInputBase
    | o <= 1             = Left InvalidOutputBase
    | any (< 0) ds       = Left (InvalidDigit (head $ filter (< 0) ds))
    | any (> i - 1) ds   = Left (InvalidDigit (head $ filter (> i - 1) ds))
    | otherwise          = Right (convert (compute ds i) o 0)

zipWithIndex :: Integral a => [a] -> [(a, Int)]
zipWithIndex xs = zip xs [0..length xs]

compute :: Integral a => [a] -> a -> a
compute digits base = sum $ map (\(d, p) -> d * base ^ p) ((zipWithIndex . reverse) digits)

convert :: Integral a => a -> a -> a -> [a]
convert 0 _ _ = []
convert v b i = convert (v `div` b) b (i + 1) ++ [v `mod` b]
