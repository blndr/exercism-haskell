module Beer (song) where

import Data.List (intercalate)

bottles :: Int -> String
bottles 1 = "1 bottle"
bottles n = (show n) ++ " bottles"

template :: Int -> String
template 0 = "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
template 1 = "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n"
template beers = bottles beers ++ " of beer on the wall, " ++ bottles beers ++ " of beer.\nTake one down and pass it around, " ++ bottles (beers - 1) ++ " of beer on the wall.\n"

song :: String
song = intercalate "\n" $ map (\n -> template n) (reverse [0..99])