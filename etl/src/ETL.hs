module ETL (transform) where

import Data.Map (Map, fromList, toList)
import Data.Char (toLower)

transform :: Map a String -> Map Char a
transform legacyData = fromList . concat $ map processInput (toList legacyData)

processInput :: (a, String) -> [(Char, a)]
processInput (a, letters) = map (makeOutput a) letters

makeOutput :: a -> Char -> (Char, a)
makeOutput a letter = (toLower letter, a)
