module RunLength (decode, encode) where

import Data.Char (isDigit)

decode :: String -> String
decode []  = []
decode (x:xs)
    | isDigit x = decodePart (x:xs) ++ decode (remainingCipher xs)
    | otherwise = x : decode xs

encode :: String -> String
encode [] = []
encode plain = encodePart (fst $ splitPlain plain) ++ encode (snd $ splitPlain plain)

decodePart :: String -> String
decodePart cipher = replicate (parseNumber cipher) (nextChar cipher)

encodePart :: String -> String
encodePart [x] = [x]
encodePart plain = show (length plain) ++ [head plain]

parseNumber :: String -> Int
parseNumber cipher = read $ takeWhile isDigit cipher

nextChar :: String -> Char
nextChar cipher = head $ dropWhile isDigit cipher

splitPlain :: String -> (String, String)
splitPlain plain = span (== (head plain)) plain

remainingCipher :: String -> String
remainingCipher cipher = tail $ dropWhile isDigit cipher
