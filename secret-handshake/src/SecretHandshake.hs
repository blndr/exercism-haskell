module SecretHandshake (handshake) where

handshake :: Int -> [String]
handshake n
    | n > 15 = reverse $ generateActions n
    | otherwise = generateActions n

generateActions :: Int -> [String]
generateActions n = concatMap action (zip [0..4] (reverse $ binary n ""))

action :: (Int, Char) -> [String]
action (p, q)
    | q == '0'  = []
    | p == 0    = ["wink"]
    | p == 1    = ["double blink"]
    | p == 2    = ["close your eyes"]
    | p == 3    = ["jump"]
    | p == 4    = []
    | otherwise = []

binary :: Int -> String -> String
binary n b
    | n > 1     = binary (n `div` 2) ((show $ n `mod` 2) ++ b)
    | otherwise = show n ++ b
