module Luhn (isValid) where

import Data.Char (digitToInt, isSpace)

isValid :: String -> Bool
isValid xs
    | length xs' <= 1 = False
    | otherwise = (sum doubles) `mod` 10 == 0
    where
        xs' = filter (not . isSpace) xs
        asInts = map digitToInt (reverse xs')
        doubles = map maybeDouble (zip [0..] asInts)

maybeDouble :: (Int, Int) -> Int
maybeDouble (i, d)
    | i `mod` 2 == 0 = d
    | otherwise      = if d * 2 > 9 then d * 2 - 9 else d * 2
