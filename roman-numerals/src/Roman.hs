module Roman (numerals) where

import Data.Maybe (catMaybes)

reference :: [(Integer, String)]
reference = [(1000, "M"), (900, "CM"), (500, "D"), (400, "CD"),
             (100, "C"), (90, "XC"), (50, "L"), (40, "XL"),
             (10, "X"), (9, "IX"), (5, "V"), (4, "IV"), (1, "I")]

largest :: Integer -> [(Integer, String)] -> Integer
largest _ [] = 0
largest n (x:xs) = if n >= fst x then fst x else largest n xs

numerals :: Integer -> Maybe String
numerals 0 = Just ""
numerals n = Just $ (concat . catMaybes) ((lookup threshold reference) : [numerals remainer])
    where
        threshold = largest n reference
        remainer = n - threshold
