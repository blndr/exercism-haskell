module BinarySearch (find) where

import Data.Array

find :: Ord a => Array Int a -> a -> Maybe Int
find a v
    | elems a == []         = Nothing
    | v == a ! (middleOf a) = Just (middleOf a)
    | v < a ! (middleOf a)  = find (subArray a (startOf a, middleOf a - 1)) v
    | v > a ! (middleOf a)  = find (subArray a ((middleOf a) + 1, endOf a)) v

middleOf :: Ord a => Array Int a -> Int
middleOf a = (startOf a) + (((endOf a) - (startOf a)) `div` 2)

startOf :: Ord a => Array Int a -> Int
startOf a = (fst . bounds) a

endOf :: Ord a => Array Int a -> Int
endOf a = (snd . bounds) a

subArray :: Ord a => Array Int a -> (Int, Int) -> Array Int a
subArray a (s, e) = array (s, e) $ filter isInRange (assocs a)
    where isInRange (i, _) = i >= s && i <= e
