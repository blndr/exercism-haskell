module IsbnVerifier (isbn) where

import Data.Char (isDigit, digitToInt)

isbn :: String -> Bool
isbn [] = False
isbn raw
    | length (intIsbn raw) /= 10                       = False
    | any (not . isDigit) (init $ filter (/= '-') raw) = False
    | otherwise = (sumOfProducts (intIsbn raw) (reverse [1..10])) `mod` 11 == 0

sumOfProducts :: [Int] -> [Int] -> Int
sumOfProducts lefts rights = sum $ map (\(a, b) -> a * b) $ zip lefts rights

intIsbn :: String -> [Int]
intIsbn [] = []
intIsbn [x]
    | x == 'X'  = [10]
    | isDigit x = [digitToInt x]
    | otherwise = []
intIsbn (x:xs) = intIsbn [x] ++ intIsbn xs