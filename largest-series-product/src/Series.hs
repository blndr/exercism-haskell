module Series (Error(..), largestProduct) where

import Data.Char (digitToInt, isDigit)

data Error = InvalidSpan | InvalidDigit Char deriving (Show, Eq)

largestProduct :: Int -> String -> Either Error Integer
largestProduct size digits
    | any (not . isDigit) digits = Left (InvalidDigit invalidChar)
    | size > length digits       = Left InvalidSpan
    | size < 0                   = Left InvalidSpan
    | otherwise                  = Right (maximum (map productAll allSubsets))
    where
        invalidChar = head $ filter (not . isDigit) digits
        integers    = map digitToInt digits
        allSubsets  = subsets size 0 integers

subsets :: Int -> Int -> [Int] -> [[Int]]
subsets size offset digits
    | size + offset > length digits = []
    | otherwise = (take size $ drop offset digits) : subsets size (offset + 1) digits

productAll :: [Int] -> Integer
productAll xs = fromIntegral $ foldl (*) 1 xs
