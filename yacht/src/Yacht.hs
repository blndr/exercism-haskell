module Yacht (yacht, Category(..)) where

import Data.List (group, sort)

data Category = Ones
              | Twos
              | Threes
              | Fours
              | Fives
              | Sixes
              | FullHouse
              | FourOfAKind
              | LittleStraight
              | BigStraight
              | Choice
              | Yacht

yacht :: Category -> [Int] -> Int
yacht Yacht ds          = if all (== head ds) (tail ds) then 50 else 0
yacht Ones ds           = sum (filter (== 1) ds)
yacht Twos ds           = sum (filter (== 2) ds)
yacht Threes ds         = sum (filter (== 3) ds)
yacht Fours ds          = sum (filter (== 4) ds)
yacht Fives ds          = sum (filter (== 5) ds)
yacht Sixes ds          = sum (filter (== 6) ds)
yacht FullHouse ds      = if isFullHouse ds then sum ds else 0
yacht FourOfAKind ds    = if isFourOfAKind ds then sum (getFourOfAKind ds) else 0    
yacht LittleStraight ds = if sort ds == [1, 2, 3, 4, 5] then 30 else 0
yacht BigStraight ds    = if sort ds == [2, 3, 4, 5, 6] then 30 else 0
yacht Choice ds         = sum ds

groups :: [Int] -> [[Int]]
groups ds = (group . sort) ds

isFullHouse :: [Int] -> Bool
isFullHouse ds = hasPair && hasBrelan
    where
       hasPair   = any (\g -> length g == 2) (groups ds)
       hasBrelan = any (\g -> length g == 3) (groups ds)

isFourOfAKind :: [Int] -> Bool
isFourOfAKind ds = any (\g -> length g >= 4) (groups ds)

getFourOfAKind :: [Int] -> [Int]
getFourOfAKind ds = take 4 $ head $ filter (\g -> length g >= 4) (groups ds)
