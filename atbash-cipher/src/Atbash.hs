module Atbash (decode, encode) where

import Data.Char (toLower, isLetter, isAlphaNum, isSpace, isDigit)

decode :: String -> String
decode cipherText = map decodeChar $ filter isAlphaNum cipherText

encode :: String -> String
encode plainText = splitCipher $ map encodeChar $ filter isAlphaNum plainText

decodeChar :: Char -> Char
decodeChar c
    | isDigit c = c
    | otherwise = ['a'..'z'] !! (length [toLower c..'z'] - 1)

encodeChar :: Char -> Char
encodeChar c
    | isDigit c = c
    | otherwise = (reverse ['a'..'z']) !! (length ['a'..toLower c] - 1)

splitCipher :: String -> String
splitCipher cipher
    | length cipher <= 5 = cipher
    | otherwise          = take 5 cipher ++ " " ++ splitCipher (drop 5 cipher)
