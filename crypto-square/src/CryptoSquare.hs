module CryptoSquare (encode) where

import Data.Char (toLower, isSpace, isAlphaNum)
import Data.List (intercalate)

encode :: String -> String
encode text = (unsquare . (chunksOf n) . (rectangleWidth 1)) n 
    where n = normalize text

normalize :: String -> String
normalize text = map toLower $ filter (\c -> isAlphaNum c && (not . isSpace) c) text

unsquare :: [String] -> String
unsquare words = intercalate " " $ map (\i -> map (\w -> w !! i) words) [0..columns - 1]
    where columns = length (words !! 0)

chunksOf :: String -> Int -> [String]
chunksOf text n
    | length text <= n = [text ++ replicate (n - (length text)) ' ']
    | otherwise        = take n text : chunksOf (drop n text) n

rectangleWidth :: Int -> String -> Int
rectangleWidth n text
    | t `div` n < n                    = n
    | t `div` n == n && t `mod` n == 0 = n
    | otherwise                        = rectangleWidth (n + 1) text
    where
        t = length text